def f(x):
    return 4*x-x**3

a = 0
b = 2
n = 5

h = (b-a)/n

s = 0
for i in range(1, int(n)+1):
    if i == 0 or i == n:
        m = 0.5
    else:
        m = 1
    s += m*f(a+i*h)

print(abs(h * s))
