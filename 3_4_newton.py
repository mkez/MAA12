from sympy import *
x = symbols('x')

f = sympify(input("f(x) = "))

def g(val):
    return (x - f / diff(f, x)).subs(x, val)

val = float(input("starting value: "))
for i in range(1, 11):
    val = g(val)
    print(val)
