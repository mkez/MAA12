from sympy import *
x = symbols('x')

f = sympify(input("f(x) = "))
a = float(input("   a = "))
b = float(input("   b = "))

while abs(b - a) > 1e-3:
    c = (a + b) / 2
    if f.subs(x, a) * f.subs(x, c) < 0:
        b = c
    else:
        a = c

print(c)
