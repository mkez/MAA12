from sympy import *
x = symbols('x')

f = sympify(input("f(x) = "))
a = float(input("   a = "))
b = float(input("   b = "))
n = float(input("   n = "))

d = (b-a)/n

s = 0
for i in range(1, int(n)+1):
    s += f.subs(x, a+ i*d - d/2)

print(abs(d * s))
