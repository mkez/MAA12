func = input("f(x) = ")
a = float(input("   a = "))
b = float(input("   b = "))
n = int(input("   n = "))

def f(x):
    return eval(func)

h = (b-a)/n

s = 0
for i in range(0, n+1):
    if i == 0 or i == n:
        m = 1
    elif i % 2 != 0:
        m = 4
    else:
        m = 2

    s += m*f(a+i*h)

print(h/3*s)
